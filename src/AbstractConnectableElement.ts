import * as CubittCommon from "cubitt-common";

import {AbstractElement} from "./AbstractElement";
import {Connector} from "./Connector";
import {Edge} from "./Edge";

export abstract class AbstractConnectableElement extends AbstractElement{

	protected _Connectors: CubittCommon.Dictionary<Connector>;

	constructor(
		Id: CubittCommon.Guid,
		Type: string,
		Properties: CubittCommon.Dictionary<any> = {},
		public Connectors: CubittCommon.Dictionary<Connector> = {}
	) {
			super(Id, Type, Properties);
			this._Connectors = Connectors;
	}

	AddConnector(connector: Connector): void {
		this._Connectors[connector.Id.ToString()] = connector;
	}

	RemoveConnector(guid: CubittCommon.Guid): void {
		delete this._Connectors[guid.ToString()];
	}
}
