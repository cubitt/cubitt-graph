import * as CubittCommon from "cubitt-common";

import {Connector} from "./Connector";

export abstract class AbstractElement {

	protected _Id: CubittCommon.Guid;
	protected _Type: string;
	protected _Properties: CubittCommon.Dictionary<any>;

	constructor(
		Id: CubittCommon.Guid,
		public Type: string,
		public Properties: CubittCommon.Dictionary<any> = {}
	) {
			this._Id = Id;
			this._Type = Type;
			this._Properties = Properties;
	}

	 get Id(): CubittCommon.Guid {
		return this._Id;
	}
}
