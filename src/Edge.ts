import * as CubittCommon from "cubitt-common";

import {Connector} from "./Connector";
import {Graph} from "./Graph";
import {Node} from "./Node";

export class Edge extends Node {
	constructor(
		Id: CubittCommon.Guid,
		Type: string,
		Properties: CubittCommon.Dictionary<any> = {},
		Connectors: CubittCommon.Dictionary<Connector> = {},
		StartConnector: Connector,
		EndConnector: Connector
	) {
			super(Id, Type, Properties, Connectors);
			this.AddConnector(StartConnector);
			StartConnector.AddEdge(this);
			this.AddConnector(EndConnector);
			EndConnector.AddEdge(this);
	}
}
