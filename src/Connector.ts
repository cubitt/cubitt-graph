import * as CubittCommon from "cubitt-common";

import {AbstractConnectableElement} from "./AbstractConnectableElement";
import {AbstractElement} from "./AbstractElement";
import {Edge} from "./Edge";
import {Node} from "./Node";

export class Connector extends AbstractElement{

	private _Edges: CubittCommon.Dictionary<Edge> = {};
	private _Parent: Node;

	constructor(
		id: CubittCommon.Guid,
		type: string,
		properties: CubittCommon.Dictionary<any> = {},
		parent: Node
	) {
        super(id, type, properties);
        this._Parent = parent;
        parent.AddConnector(this);
	}

	get Edges(): CubittCommon.Dictionary<Edge> {
		return this._Edges;
	}

	AddEdge(edge: Edge): void {
		this._Edges[edge.Id.ToString()] = edge;
	}

	RemoveEdge(guid: CubittCommon.Guid): void {
		delete this._Edges[guid.ToString()];
	}

	get Parent(): Node {
		return this._Parent;
	}
}
