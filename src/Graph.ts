import * as CubittCommon from "cubitt-common";

import {AbstractConnectableElement} from "./AbstractConnectableElement";
import {Common} from "./Common";
import {Connector} from "./Connector";

export class Graph extends AbstractConnectableElement {
	constructor(
		Id: CubittCommon.Guid,
		Type: string,
		Properties: CubittCommon.Dictionary<any> = {},
		Connectors: CubittCommon.Dictionary<Connector> = {}
	) {
			super(Id, Type, Properties, Connectors);
	}

	GetChildConnector(): Connector {
		for (var guid in this.Connectors) {
			if (this.Connectors[guid].Type == "ChildConnector") {
				return this.Connectors[guid];
			}
		}
	}
}
