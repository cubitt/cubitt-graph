import * as CubittCommon from "cubitt-common";

import {AbstractElement} from "./AbstractElement";

export class Common {
	static FindElementsByAttribute<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, attribute: string, value: any): CubittCommon.Dictionary<T> {
		var _output: CubittCommon.Dictionary<T> =  {};
		for (var guid in dictionary) {
			if (dictionary[guid][attribute] == value) {
				_output[guid] = dictionary[guid];
			}
		}
		return _output;
	}

	static FindElementsByType<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, type: string): CubittCommon.Dictionary<T> {
		return this.FindElementsByAttribute<T>(dictionary, "Type", type);
	}

	static FindElementById<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, id: string): T {
		for (var guid in dictionary) {
			if (dictionary[guid].Id.ToString() == id) {
				return dictionary[guid];
			}
		}
	}

	static FindElementsByProperty<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, property: string, value: any): CubittCommon.Dictionary<T> {
		var _output: CubittCommon.Dictionary<T> = {};
		for (var guid in dictionary) {
			for (var PropertyKey in dictionary[guid].Properties) {
				if (dictionary[guid].Properties[PropertyKey] == value) {
					_output[guid] = dictionary[guid];
				}
			}
		}
		return _output;
	}
}
