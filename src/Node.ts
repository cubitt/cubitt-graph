import * as CubittCommon from "cubitt-common";

import {AbstractConnectableElement} from "./AbstractConnectableElement";
import {Common} from "./Common";
import {Connector} from "./Connector";
import {Edge} from "./Edge";

export class Node extends AbstractConnectableElement {
	constructor(
		Id: CubittCommon.Guid,
		Type: string,
		Properties: CubittCommon.Dictionary<any> = {},
		Connectors: CubittCommon.Dictionary<Connector> = {}
	) {
			super(Id, Type, Properties, Connectors);
	}

	get ParentConnector(): Connector {
		for (var guid in this.Connectors) {
			if (this.Connectors[guid].Type == "ParentConnector") {
				return this.Connectors[guid];
			}
		}
	}

	DeleteParentConnector(): void {
		for (var guid in this.Connectors) {
			if (this.Connectors[guid].Type == "ParentConnector") {
				delete this.Connectors[guid];
			}
		}
	}
}
