import * as CubittCommon from "cubitt-common";

import {AbstractElement} from "./AbstractElement";
import {Common} from "./Common";
import {Connector} from "./Connector";
import {Edge} from "./Edge";
import {Graph} from "./Graph";
import {Node} from "./Node";

export class Project extends AbstractElement {

	private _AllConnectors: CubittCommon.Dictionary<Connector>;
	private _AllEdges: CubittCommon.Dictionary<Edge>;
	private _AllGraphs: CubittCommon.Dictionary<Graph>;
	private _AllNodes: CubittCommon.Dictionary<Node>;

	private FindConnector(guid: CubittCommon.Guid): Connector {
		var temp: Connector = this._AllConnectors[guid.ToString()];
		if (typeof temp == 'undefined') {
			throw new Error("ERROR: Connector with GUID " + guid.ToString() + " does not exist!");
		}
		return temp;
	}

	private FindEdge(guid: CubittCommon.Guid): Edge {
		var temp: Edge = this._AllEdges[guid.ToString()];
		if (typeof temp == 'undefined') {
			throw new Error("ERROR: Edge with GUID " + guid.ToString() + " does not exist!");
		}
		return temp;
	}

	private FindGraph(guid: CubittCommon.Guid): Graph {
		var temp: Graph = this._AllGraphs[guid.ToString()];
		if (typeof temp == 'undefined') {
			throw new Error("ERROR: Graph with GUID " + guid.ToString() + " does not exist!");
		}
		return temp;
	}

	private FindNode(guid: CubittCommon.Guid): Node {
		var temp: Node = this._AllNodes[guid.ToString()];
		if (typeof temp == 'undefined') {
			throw new Error("ERROR: Node with GUID " + guid.ToString() + " does not exist!");
		}
		return temp;
	}

	private CheckGuidUnique(guid: CubittCommon.Guid): boolean {
		var strGuid: string = guid.ToString();
		if (
			typeof this._AllConnectors[strGuid] == 'undefined' &&
			typeof this._AllEdges[strGuid] == 'undefined' &&
			typeof this._AllGraphs[strGuid] == 'undefined' &&
			typeof this._AllNodes[strGuid] == 'undefiend' &&
			this.Id.ToString() != strGuid
		) {
			return true;
		} else {
			return false;
		}
	}

	private GenerateUniqueGuid(): CubittCommon.Guid {
		var newGuid: CubittCommon.Guid = CubittCommon.Guid.newGuid();
		while (!this.CheckGuidUnique(newGuid)) {
			newGuid = CubittCommon.Guid.newGuid();
		}
		return newGuid;
	}

	AddConnector(
		id: CubittCommon.Guid,
		type: string,
		properties: CubittCommon.Dictionary<any> = {},
		edges: CubittCommon.Guid[] = [],
		parent: CubittCommon.Guid
	): void {
		// id validation
		if (!this.CheckGuidUnique(id)) {
			throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
		}

		// type validation

		// properties validation

		// parent validation
		var validParent: Node = this.FindNode(parent);

		// generate connector
		var newConnector: Connector = new Connector(id, type, properties, validParent);

		// finally add the connector
		this._AllConnectors[id.ToString()] = newConnector;
	}

	AddEdge(
		id: CubittCommon.Guid,
		type: string,
		properties: CubittCommon.Dictionary<any> = {},
		connectors: CubittCommon.Guid[] = [],
		parent: CubittCommon.Guid,
		startConnector: CubittCommon.Guid,
		endConnector: CubittCommon.Guid
	): void {
		// id validation
		if (!this.CheckGuidUnique(id)) {
			throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
		}

		// type validation

		// properties validation

		// connectors validation
		var validConnectors: CubittCommon.Dictionary<Connector> = {};
		for (var i = 0; i < connectors.length; i++) {
			validConnectors[connectors[i].ToString()] = this.FindConnector(connectors[i]);
		}

		// parent validation
		var validParent: Graph = this.FindGraph(parent);

		// startConnector validation
		var start: Connector = this.FindConnector(startConnector);

		// endConnector validation
		var end: Connector = this.FindConnector(endConnector);

		// generate edge and add to project
		var newEdge: Edge = new Edge(id, type, properties, validConnectors, start, end);
		this._AllEdges[id.ToString()] = newEdge;

		// add parent connector to the new edge and project
		var newConnector = new Connector(this.GenerateUniqueGuid(), "parentConnector", {}, newEdge);
		newEdge.AddConnector(newConnector);
		this._AllConnectors[newConnector.Id.ToString()] = newConnector;

		// find child connector on parent
		var graphConnector: Connector = validParent.GetChildConnector();

		// make new edge between the two and add it to the project
		var newEdge: Edge = new Edge(this.GenerateUniqueGuid(), "Contains",	{}, {}, graphConnector,	newConnector);
		this._AllEdges[newEdge.Id.ToString()] = newEdge;
	}

	AddNode(
		id: CubittCommon.Guid,
		type: string,
		properties: CubittCommon.Dictionary<any> = {},
		connectors: CubittCommon.Guid[] = [],
		parent: CubittCommon.Guid
	):void {
		// id validation
		if (!this.CheckGuidUnique(id)) {
			throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
		}

		// type validation

		// parent validation
		var validParent: Graph = this.FindGraph(parent);

		// properties validation

		// connectors validation
		var validConnectors: CubittCommon.Dictionary<Connector> = {};
		for (var i = 0; i < connectors.length; i++) {
			validConnectors[connectors[i].ToString()] = this.FindConnector(connectors[i]);
		}

		// generate node and add to project
		var newNode: Node = new Node(id, type, properties, validConnectors);
		this._AllNodes[id.ToString()] = newNode;

		// add parent connector to the new node and project
		var newConnector: Connector = new Connector(this.GenerateUniqueGuid(), "ParentConnector", {}, newNode);
		newNode.AddConnector(newConnector);
		this._AllConnectors[newConnector.Id.ToString()] = newConnector;

		// find child connector on parent
		var graphConnector: Connector = validParent.GetChildConnector();

		// make new edge between the two and add it to the project
		var newEdge: Edge = new Edge(this.GenerateUniqueGuid(), "Contains",	{}, {}, graphConnector,	newConnector);
		this._AllEdges[newEdge.Id.ToString()] = newEdge;
	}

	AddGraph(): void {

	}

	DeleteConnector(
		id: CubittCommon.Guid
	): void {
		var toDeleteConnector = this.FindConnector(id);
		toDeleteConnector.Parent.RemoveConnector(id);
		for (var guid in toDeleteConnector.Edges) {
			this.DeleteEdge(guid);
		}
		delete this._AllConnectors[id.ToString()];
		// now it should be garbage collected because there should not be any pointers left pointing to it
	}

	DeleteEdge(
		id: CubittCommon.Guid
	): void {
		var toDeleteEdge: Edge = this.FindEdge(id);
		if (toDeleteEdge.Type = "Contains") { // just remove the edge
			for (var guid in toDeleteEdge.Connectors) {
				toDeleteEdge.RemoveConnector(guid);
			}
			delete this._AllEdges[id.ToString()];
		} else {
			var containsEdges: CubittCommon.Dictionary<Edge> = Common.FindElementsByType(toDeleteEdge.ParentConnector.Edges, "Contains");
			for (var guid in containsEdges) { // remove edges connecting it to any graph
				this.DeleteEdge(guid);
			}
			toDeleteEdge.RemoveConnector(toDeleteEdge.ParentConnector.Id); // remove its parent connector so it doesn't point back
			for (var guid in toDeleteEdge.Connectors) { // remove it from any connectors it is still connected to
				toDeleteEdge.Connectors[guid].RemoveEdge(id);
			}
		}
		// remove it from the global dictionary
		delete this._AllEdges[id.ToString()];
		// now it should be garbage collected because there should not be any pointers left pointing to it
	}

	DeleteGraph(): void {

	}

	DeleteNode(
		id: CubittCommon.Guid
	) {
		var toDeleteNode: Node = this.FindNode(id);
		for (var connectorGuid in toDeleteNode.Connectors) {
			for (var edgeGuid in toDeleteNode.Connectors[connectorGuid].Edges) {
				this.DeleteEdge(edgeGuid);
			}
		}
	}

	DeleteConnectorProperty(
		id: CubittCommon.Guid,
		key: string
	): void {
		var validConnector: Connector = this.FindConnector(id);
		delete validConnector.Properties[key];
	}

	DeleteEdgeProperty(
		id: CubittCommon.Guid,
		key: string
	): void {
		var validEdge: Edge = this.FindEdge(id);
		delete validEdge.Properties[key];
	}

	DeleteGraphProperty(
		id: CubittCommon.Guid,
		key: string
	): void {
		var validGraph: Graph = this.FindGraph(id);
		delete validGraph.Properties[key];
	}

	DeleteNodeProperty(
		id: CubittCommon.Guid,
		key: string
	): void {
		var validNode: Node = this.FindNode(id);
		delete validNode.Properties[key];
	}

	DeleteProjectProperty(
		key: string
	): void {
		delete this.Properties[key];
	}

	SetConnectorType(
		id: CubittCommon.Guid,
		type: string
	): void {
		var validConnector: Connector = this.FindConnector(id);
		validConnector.Type = type;
	}

	SetEdgeType(
		id: CubittCommon.Guid,
		type: string
	): void {
		var validEdge: Edge = this.FindEdge(id);
		validEdge.Type = type;
	}

	SetGraphType(
		id: CubittCommon.Guid,
		type: string
	): void {
		var validGraph: Graph = this.FindGraph(id);
		validGraph.Type = type;
	}

	SetNodeType(
		id: CubittCommon.Guid,
		type: string
	): void {
		var validNode: Node = this.FindNode(id);
		validNode.Type = type;
	}

	SetProjectType(
		type: string
	): void {
		this.Type = type;
	}

	SetConnectorProperty(
		id: CubittCommon.Guid,
		key: string,
		value: string
	): void {
		var validConnector: Connector = this.FindConnector(id);
		validConnector.Properties[key] = value;
	}

	SetEdgeProperty(
		id: CubittCommon.Guid,
		key: string,
		value: string
	): void {
		var validEdge: Edge = this.FindEdge(id);
		validEdge.Properties[key] = value;
	}

	SetGraphProperty(
		id: CubittCommon.Guid,
		key: string,
		value: string
	): void {
		var validGraph: Graph = this.FindGraph(id);
		validGraph.Properties[key] = value;
	}

	SetNodeProperty(
		id: CubittCommon.Guid,
		key: string,
		value: string
	): void {
		var validNode: Node = this.FindNode(id);
		validNode.Properties[key] = value;
	}

	SetProjectProperty(
		key: string,
		value: string
	): void {
		this.Properties[key] = value;
	}
}
