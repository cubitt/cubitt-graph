# Cubitt-graph
Graph engine implementation for the Cubitt framework. More information coming soon.

Run `npm install` in this project and then open it with [`atom-typescript`](https://atom.io/packages/atom-typescript) and you can see it gets picked up automatically.

## How to generate documentation
Run the following commands:
> npm install --dev

Then run:
> node_modules/.bin/typedoc --out doc/ --module commonjs --target ES5 --mode file  src/

Now you can view the documentation by opening the doc/index.html file using your browser.
