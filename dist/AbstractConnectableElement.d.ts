import * as CubittCommon from "cubitt-common";
import { AbstractElement } from "./AbstractElement";
import { Connector } from "./Connector";
export declare abstract class AbstractConnectableElement extends AbstractElement {
    Connectors: CubittCommon.Dictionary<Connector>;
    protected _Connectors: CubittCommon.Dictionary<Connector>;
    constructor(Id: CubittCommon.Guid, Type: string, Properties?: CubittCommon.Dictionary<any>, Connectors?: CubittCommon.Dictionary<Connector>);
    AddConnector(connector: Connector): void;
    RemoveConnector(guid: CubittCommon.Guid): void;
}
