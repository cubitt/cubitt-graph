import * as CubittCommon from "cubitt-common";
import { AbstractElement } from "./AbstractElement";
import { Edge } from "./Edge";
import { Node } from "./Node";
export declare class Connector extends AbstractElement {
    private _Edges;
    private _Parent;
    constructor(id: CubittCommon.Guid, type: string, properties: CubittCommon.Dictionary<any>, parent: Node);
    Edges: CubittCommon.Dictionary<Edge>;
    AddEdge(edge: Edge): void;
    RemoveEdge(guid: CubittCommon.Guid): void;
    Parent: Node;
}
