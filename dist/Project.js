"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CubittCommon = require("cubitt-common");
var AbstractElement_1 = require("./AbstractElement");
var Common_1 = require("./Common");
var Connector_1 = require("./Connector");
var Edge_1 = require("./Edge");
var Node_1 = require("./Node");
var Project = (function (_super) {
    __extends(Project, _super);
    function Project() {
        _super.apply(this, arguments);
    }
    Project.prototype.FindConnector = function (guid) {
        var temp = this._AllConnectors[guid.ToString()];
        if (typeof temp == 'undefined') {
            throw new Error("ERROR: Connector with GUID " + guid.ToString() + " does not exist!");
        }
        return temp;
    };
    Project.prototype.FindEdge = function (guid) {
        var temp = this._AllEdges[guid.ToString()];
        if (typeof temp == 'undefined') {
            throw new Error("ERROR: Edge with GUID " + guid.ToString() + " does not exist!");
        }
        return temp;
    };
    Project.prototype.FindGraph = function (guid) {
        var temp = this._AllGraphs[guid.ToString()];
        if (typeof temp == 'undefined') {
            throw new Error("ERROR: Graph with GUID " + guid.ToString() + " does not exist!");
        }
        return temp;
    };
    Project.prototype.FindNode = function (guid) {
        var temp = this._AllNodes[guid.ToString()];
        if (typeof temp == 'undefined') {
            throw new Error("ERROR: Node with GUID " + guid.ToString() + " does not exist!");
        }
        return temp;
    };
    Project.prototype.CheckGuidUnique = function (guid) {
        var strGuid = guid.ToString();
        if (typeof this._AllConnectors[strGuid] == 'undefined' &&
            typeof this._AllEdges[strGuid] == 'undefined' &&
            typeof this._AllGraphs[strGuid] == 'undefined' &&
            typeof this._AllNodes[strGuid] == 'undefiend' &&
            this.Id.ToString() != strGuid) {
            return true;
        }
        else {
            return false;
        }
    };
    Project.prototype.GenerateUniqueGuid = function () {
        var newGuid = CubittCommon.Guid.newGuid();
        while (!this.CheckGuidUnique(newGuid)) {
            newGuid = CubittCommon.Guid.newGuid();
        }
        return newGuid;
    };
    Project.prototype.AddConnector = function (id, type, properties, edges, parent) {
        if (properties === void 0) { properties = {}; }
        if (edges === void 0) { edges = []; }
        if (!this.CheckGuidUnique(id)) {
            throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
        }
        var validParent = this.FindNode(parent);
        var newConnector = new Connector_1.Connector(id, type, properties, validParent);
        this._AllConnectors[id.ToString()] = newConnector;
    };
    Project.prototype.AddEdge = function (id, type, properties, connectors, parent, startConnector, endConnector) {
        if (properties === void 0) { properties = {}; }
        if (connectors === void 0) { connectors = []; }
        if (!this.CheckGuidUnique(id)) {
            throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
        }
        var validConnectors = {};
        for (var i = 0; i < connectors.length; i++) {
            validConnectors[connectors[i].ToString()] = this.FindConnector(connectors[i]);
        }
        var validParent = this.FindGraph(parent);
        var start = this.FindConnector(startConnector);
        var end = this.FindConnector(endConnector);
        var newEdge = new Edge_1.Edge(id, type, properties, validConnectors, start, end);
        this._AllEdges[id.ToString()] = newEdge;
        var newConnector = new Connector_1.Connector(this.GenerateUniqueGuid(), "parentConnector", {}, newEdge);
        newEdge.AddConnector(newConnector);
        this._AllConnectors[newConnector.Id.ToString()] = newConnector;
        var graphConnector = validParent.GetChildConnector();
        var newEdge = new Edge_1.Edge(this.GenerateUniqueGuid(), "Contains", {}, {}, graphConnector, newConnector);
        this._AllEdges[newEdge.Id.ToString()] = newEdge;
    };
    Project.prototype.AddNode = function (id, type, properties, connectors, parent) {
        if (properties === void 0) { properties = {}; }
        if (connectors === void 0) { connectors = []; }
        if (!this.CheckGuidUnique(id)) {
            throw new Error("ERROR: GUID " + id.ToString() + " already exists!");
        }
        var validParent = this.FindGraph(parent);
        var validConnectors = {};
        for (var i = 0; i < connectors.length; i++) {
            validConnectors[connectors[i].ToString()] = this.FindConnector(connectors[i]);
        }
        var newNode = new Node_1.Node(id, type, properties, validConnectors);
        this._AllNodes[id.ToString()] = newNode;
        var newConnector = new Connector_1.Connector(this.GenerateUniqueGuid(), "ParentConnector", {}, newNode);
        newNode.AddConnector(newConnector);
        this._AllConnectors[newConnector.Id.ToString()] = newConnector;
        var graphConnector = validParent.GetChildConnector();
        var newEdge = new Edge_1.Edge(this.GenerateUniqueGuid(), "Contains", {}, {}, graphConnector, newConnector);
        this._AllEdges[newEdge.Id.ToString()] = newEdge;
    };
    Project.prototype.AddGraph = function () {
    };
    Project.prototype.DeleteConnector = function (id) {
        var toDeleteConnector = this.FindConnector(id);
        toDeleteConnector.Parent.RemoveConnector(id);
        for (var guid in toDeleteConnector.Edges) {
            this.DeleteEdge(guid);
        }
        delete this._AllConnectors[id.ToString()];
    };
    Project.prototype.DeleteEdge = function (id) {
        var toDeleteEdge = this.FindEdge(id);
        if (toDeleteEdge.Type = "Contains") {
            for (var guid in toDeleteEdge.Connectors) {
                toDeleteEdge.RemoveConnector(guid);
            }
            delete this._AllEdges[id.ToString()];
        }
        else {
            var containsEdges = Common_1.Common.FindElementsByType(toDeleteEdge.ParentConnector.Edges, "Contains");
            for (var guid in containsEdges) {
                this.DeleteEdge(guid);
            }
            toDeleteEdge.RemoveConnector(toDeleteEdge.ParentConnector.Id);
            for (var guid in toDeleteEdge.Connectors) {
                toDeleteEdge.Connectors[guid].RemoveEdge(id);
            }
        }
        delete this._AllEdges[id.ToString()];
    };
    Project.prototype.DeleteGraph = function () {
    };
    Project.prototype.DeleteNode = function (id) {
        var toDeleteNode = this.FindNode(id);
        for (var connectorGuid in toDeleteNode.Connectors) {
            for (var edgeGuid in toDeleteNode.Connectors[connectorGuid].Edges) {
                this.DeleteEdge(edgeGuid);
            }
        }
    };
    Project.prototype.DeleteConnectorProperty = function (id, key) {
        var validConnector = this.FindConnector(id);
        delete validConnector.Properties[key];
    };
    Project.prototype.DeleteEdgeProperty = function (id, key) {
        var validEdge = this.FindEdge(id);
        delete validEdge.Properties[key];
    };
    Project.prototype.DeleteGraphProperty = function (id, key) {
        var validGraph = this.FindGraph(id);
        delete validGraph.Properties[key];
    };
    Project.prototype.DeleteNodeProperty = function (id, key) {
        var validNode = this.FindNode(id);
        delete validNode.Properties[key];
    };
    Project.prototype.DeleteProjectProperty = function (key) {
        delete this.Properties[key];
    };
    Project.prototype.SetConnectorType = function (id, type) {
        var validConnector = this.FindConnector(id);
        validConnector.Type = type;
    };
    Project.prototype.SetEdgeType = function (id, type) {
        var validEdge = this.FindEdge(id);
        validEdge.Type = type;
    };
    Project.prototype.SetGraphType = function (id, type) {
        var validGraph = this.FindGraph(id);
        validGraph.Type = type;
    };
    Project.prototype.SetNodeType = function (id, type) {
        var validNode = this.FindNode(id);
        validNode.Type = type;
    };
    Project.prototype.SetProjectType = function (type) {
        this.Type = type;
    };
    Project.prototype.SetConnectorProperty = function (id, key, value) {
        var validConnector = this.FindConnector(id);
        validConnector.Properties[key] = value;
    };
    Project.prototype.SetEdgeProperty = function (id, key, value) {
        var validEdge = this.FindEdge(id);
        validEdge.Properties[key] = value;
    };
    Project.prototype.SetGraphProperty = function (id, key, value) {
        var validGraph = this.FindGraph(id);
        validGraph.Properties[key] = value;
    };
    Project.prototype.SetNodeProperty = function (id, key, value) {
        var validNode = this.FindNode(id);
        validNode.Properties[key] = value;
    };
    Project.prototype.SetProjectProperty = function (key, value) {
        this.Properties[key] = value;
    };
    return Project;
}(AbstractElement_1.AbstractElement));
exports.Project = Project;
