"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractConnectableElement_1 = require("./AbstractConnectableElement");
var Node = (function (_super) {
    __extends(Node, _super);
    function Node(Id, Type, Properties, Connectors) {
        if (Properties === void 0) { Properties = {}; }
        if (Connectors === void 0) { Connectors = {}; }
        _super.call(this, Id, Type, Properties, Connectors);
    }
    Object.defineProperty(Node.prototype, "ParentConnector", {
        get: function () {
            for (var guid in this.Connectors) {
                if (this.Connectors[guid].Type == "ParentConnector") {
                    return this.Connectors[guid];
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Node.prototype.DeleteParentConnector = function () {
        for (var guid in this.Connectors) {
            if (this.Connectors[guid].Type == "ParentConnector") {
                delete this.Connectors[guid];
            }
        }
    };
    return Node;
}(AbstractConnectableElement_1.AbstractConnectableElement));
exports.Node = Node;
