import * as CubittCommon from "cubitt-common";
import { Connector } from "./Connector";
import { Node } from "./Node";
export declare class Edge extends Node {
    constructor(Id: CubittCommon.Guid, Type: string, Properties: CubittCommon.Dictionary<any>, Connectors: CubittCommon.Dictionary<Connector>, StartConnector: Connector, EndConnector: Connector);
}
