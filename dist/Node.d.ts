import * as CubittCommon from "cubitt-common";
import { AbstractConnectableElement } from "./AbstractConnectableElement";
import { Connector } from "./Connector";
export declare class Node extends AbstractConnectableElement {
    constructor(Id: CubittCommon.Guid, Type: string, Properties?: CubittCommon.Dictionary<any>, Connectors?: CubittCommon.Dictionary<Connector>);
    ParentConnector: Connector;
    DeleteParentConnector(): void;
}
