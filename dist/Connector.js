"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractElement_1 = require("./AbstractElement");
var Connector = (function (_super) {
    __extends(Connector, _super);
    function Connector(id, type, properties, parent) {
        if (properties === void 0) { properties = {}; }
        _super.call(this, id, type, properties);
        this._Edges = {};
        this._Parent = parent;
        parent.AddConnector(this);
    }
    Object.defineProperty(Connector.prototype, "Edges", {
        get: function () {
            return this._Edges;
        },
        enumerable: true,
        configurable: true
    });
    Connector.prototype.AddEdge = function (edge) {
        this._Edges[edge.Id.ToString()] = edge;
    };
    Connector.prototype.RemoveEdge = function (guid) {
        delete this._Edges[guid.ToString()];
    };
    Object.defineProperty(Connector.prototype, "Parent", {
        get: function () {
            return this._Parent;
        },
        enumerable: true,
        configurable: true
    });
    return Connector;
}(AbstractElement_1.AbstractElement));
exports.Connector = Connector;
