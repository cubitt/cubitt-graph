"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractElement_1 = require("./AbstractElement");
var AbstractConnectableElement = (function (_super) {
    __extends(AbstractConnectableElement, _super);
    function AbstractConnectableElement(Id, Type, Properties, Connectors) {
        if (Properties === void 0) { Properties = {}; }
        if (Connectors === void 0) { Connectors = {}; }
        _super.call(this, Id, Type, Properties);
        this.Connectors = Connectors;
        this._Connectors = Connectors;
    }
    AbstractConnectableElement.prototype.AddConnector = function (connector) {
        this._Connectors[connector.Id.ToString()] = connector;
    };
    AbstractConnectableElement.prototype.RemoveConnector = function (guid) {
        delete this._Connectors[guid.ToString()];
    };
    return AbstractConnectableElement;
}(AbstractElement_1.AbstractElement));
exports.AbstractConnectableElement = AbstractConnectableElement;
