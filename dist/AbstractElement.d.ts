import * as CubittCommon from "cubitt-common";
export declare abstract class AbstractElement {
    Type: string;
    Properties: CubittCommon.Dictionary<any>;
    protected _Id: CubittCommon.Guid;
    protected _Type: string;
    protected _Properties: CubittCommon.Dictionary<any>;
    constructor(Id: CubittCommon.Guid, Type: string, Properties?: CubittCommon.Dictionary<any>);
    Id: CubittCommon.Guid;
}
