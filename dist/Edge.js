"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Node_1 = require("./Node");
var Edge = (function (_super) {
    __extends(Edge, _super);
    function Edge(Id, Type, Properties, Connectors, StartConnector, EndConnector) {
        if (Properties === void 0) { Properties = {}; }
        if (Connectors === void 0) { Connectors = {}; }
        _super.call(this, Id, Type, Properties, Connectors);
        this.AddConnector(StartConnector);
        StartConnector.AddEdge(this);
        this.AddConnector(EndConnector);
        EndConnector.AddEdge(this);
    }
    return Edge;
}(Node_1.Node));
exports.Edge = Edge;
