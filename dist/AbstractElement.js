"use strict";
var AbstractElement = (function () {
    function AbstractElement(Id, Type, Properties) {
        if (Properties === void 0) { Properties = {}; }
        this.Type = Type;
        this.Properties = Properties;
        this._Id = Id;
        this._Type = Type;
        this._Properties = Properties;
    }
    Object.defineProperty(AbstractElement.prototype, "Id", {
        get: function () {
            return this._Id;
        },
        enumerable: true,
        configurable: true
    });
    return AbstractElement;
}());
exports.AbstractElement = AbstractElement;
