import * as CubittCommon from "cubitt-common";
import { AbstractElement } from "./AbstractElement";
export declare class Common {
    static FindElementsByAttribute<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, attribute: string, value: any): CubittCommon.Dictionary<T>;
    static FindElementsByType<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, type: string): CubittCommon.Dictionary<T>;
    static FindElementById<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, id: string): T;
    static FindElementsByProperty<T extends AbstractElement>(dictionary: CubittCommon.Dictionary<T>, property: string, value: any): CubittCommon.Dictionary<T>;
}
