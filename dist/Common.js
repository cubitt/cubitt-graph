"use strict";
var Common = (function () {
    function Common() {
    }
    Common.FindElementsByAttribute = function (dictionary, attribute, value) {
        var _output = {};
        for (var guid in dictionary) {
            if (dictionary[guid][attribute] == value) {
                _output[guid] = dictionary[guid];
            }
        }
        return _output;
    };
    Common.FindElementsByType = function (dictionary, type) {
        return this.FindElementsByAttribute(dictionary, "Type", type);
    };
    Common.FindElementById = function (dictionary, id) {
        for (var guid in dictionary) {
            if (dictionary[guid].Id.ToString() == id) {
                return dictionary[guid];
            }
        }
    };
    Common.FindElementsByProperty = function (dictionary, property, value) {
        var _output = {};
        for (var guid in dictionary) {
            for (var PropertyKey in dictionary[guid].Properties) {
                if (dictionary[guid].Properties[PropertyKey] == value) {
                    _output[guid] = dictionary[guid];
                }
            }
        }
        return _output;
    };
    return Common;
}());
exports.Common = Common;
