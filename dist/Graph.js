"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractConnectableElement_1 = require("./AbstractConnectableElement");
var Graph = (function (_super) {
    __extends(Graph, _super);
    function Graph(Id, Type, Properties, Connectors) {
        if (Properties === void 0) { Properties = {}; }
        if (Connectors === void 0) { Connectors = {}; }
        _super.call(this, Id, Type, Properties, Connectors);
    }
    Graph.prototype.GetChildConnector = function () {
        for (var guid in this.Connectors) {
            if (this.Connectors[guid].Type == "ChildConnector") {
                return this.Connectors[guid];
            }
        }
    };
    return Graph;
}(AbstractConnectableElement_1.AbstractConnectableElement));
exports.Graph = Graph;
